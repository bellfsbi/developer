# FSBI Developer Interview Questions.

### Question 1
Write a program to identify when a vehicle is stopped on the route. Files to use: `gps.csv`. 

### Question 2

In this repo there is a folder/xlsx called Analytical Excersises. Please complete the questions in the repo/excel with the data included. 


### Question 3
Write a program that reads a csv file and generates SQL commands to create a table and insert the data. The program should save output to `script.sql`. Feel free to use `gps.csv` and `testresult.csv` which are located in this repo.

**Your program should pick up column names from the first row of each file.**

### Question 4

Update index.html,app.js and app.css (/frontend/) to complete the following tasks. 
#### Tasks 


1.  Display data in bargraph (Bar + Team Average line) ( See "1.png")
2.  User should get feedback on hover and mouseout events.( See "2.png")
3.  When user click on the bar of graph1 (Product vs Effectiveness) , it should filter the data and update the graph 2 (Market vs Effectiveness).( See "3.png")
4.  Extra: make graphs and page responsive for different size of displays
5.  Extra: show tooltip on hover


#### Code:

Folder : 

/frontend/app.js

/frontend/app.css

/frontend/index.html

#### Data:

/frontend/producteffectiveness.json

/frontend/productmarketeffectiveness.json


#### Libraries used:

D3  v5 ( you can change the version if you want)

Bootstrap v4.3  ( you can change the version  if you want)


#### How to run:

Open console and go to frontend 

if you have installed python 3 then use following command

`python -m http.server 3000`

if you have installed python2 then use following command

`python -m SimpleHTTPServer 8080`

if you have installed node js , then use following command

`npm install http-server -g`

`http-server .`


