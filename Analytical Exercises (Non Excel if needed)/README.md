# Analytical Interview Questions.

### Question a

Plot the total number of TV customers and Internet customers over time. What trends to you observe? Why?

**Note: the "A_customer" field is the number of customers with modem A in a particular week.  "A_troubles" field is the number of troubles customers with modem A experienced in a particular week.**

### Question b

Report rate is the number of troubles per customer. Plot the report rates for TV customers and Internet customers. What trends do you observe?

### Question c
What does the following SQL command produce?

```
select  month(week) as month,
        sum(A_Customers) as A_Customers_month,
        sum(A_Troubles) as A_Troubles_month
from FTV
where week between 01Apr2016 and 30Jun2016
group by month
```

### Question d

What does the following SQL command produce?

```
select week
from Internet
group by week
having max(A_Customers) = A_Customers

```